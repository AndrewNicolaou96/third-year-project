#version 150

in  vec2 ex_TexCoord; //texture coord arriving from the vertex
in  vec3 ex_Normal;  //normal arriving from the vertex

out vec4 out_Color;   //colour for the pixel
in vec3 ex_LightDir[3];  //light direction arriving from the vertex
//in int ex_maxLights;

in vec3 ex_PositionEye;
in vec3 ex_ToLightVector[3];

uniform vec4 light_ambient[3];
uniform vec4 light_diffuse;
uniform vec4 light_specular;
uniform vec3 attenuation[3];


uniform vec4 material_ambient;
uniform vec4 material_diffuse;
uniform vec4 material_specular;
uniform float material_shininess;
uniform int maxLights;

uniform sampler2D DiffuseMap;

void main(void)
{
	vec4 color;
	
	//Calculate lighting - multiple lights
	for(int i = 0; i < 2; i++){
		//attenuation
		float distance = length(ex_ToLightVector[i]);
		float attenuationFactor = attenuation[i].x + (attenuation[i].y * distance) + (attenuation[i].z * distance * distance);

		//directional
		vec3 n, L;
		float NdotL;
		

		//diffuse
		n = normalize(ex_Normal);
		L = normalize(ex_LightDir[i]);

		vec3 v = normalize(-ex_PositionEye);
		vec3 r = normalize(-reflect(L, n));
		
		//specular
		float RdotV = max(0.0, dot(r, v));

		NdotL = max(dot(n, L),0.0);

		color += (light_ambient[i] * material_ambient) / attenuationFactor;
	
		if(NdotL > 0.0) 
		{
			color += ((light_ambient[i] * material_diffuse * NdotL) / attenuationFactor);
		}

		color += (material_specular * light_specular * pow(RdotV, material_shininess)) / attenuationFactor;
	}

    out_Color = color * texture(DiffuseMap, ex_TexCoord); //show texture and lighting
}