#version 150

//uniform mat4 ModelViewMatrix;
uniform mat4 ProjectionMatrix;
uniform mat3 NormalMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ModelMatrix;
uniform int maxLights;

in  vec3 in_Position;  // Position coming in
in  vec2 in_TexCoord;  // texture coordinate coming in
in  vec3 in_Normal;    // vertex normal used for lighting

uniform vec4 LightPos[3];  // light position

out vec2 ex_TexCoord;  // exiting texture coord
out vec3 ex_Normal;    // exiting normal transformed by the normal matrix
out vec3 ex_PositionEye; 
out vec3 ex_LightDir[3]; 
out vec3 ex_ToLightVector[3];
//out int ex_maxLights;

void main(void)
{
	mat4 ModelViewMatrix = ViewMatrix * ModelMatrix;

	gl_Position = ProjectionMatrix * ModelViewMatrix * vec4(in_Position, 1.0);
	
	ex_TexCoord = in_TexCoord;
		
	ex_Normal = NormalMatrix*in_Normal; 

	vec3 worldPos =  vec3((ModelMatrix * vec4(in_Position, 1.0)));

	ex_PositionEye = vec3((ModelViewMatrix * vec4(in_Position, 1.0)));

	//multiple lights
	for(int i = 0; i < maxLights; i++){
		ex_LightDir[i] = vec3(ViewMatrix * LightPos[i]);
		ex_ToLightVector[i] = LightPos[i].xyz - worldPos;
	}

}