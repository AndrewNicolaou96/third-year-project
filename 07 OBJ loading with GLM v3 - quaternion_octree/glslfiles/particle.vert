#version 150

//uniform mat4 ModelViewMatrix;
uniform mat4 ProjectionMatrix;
uniform mat3 NormalMatrix;
uniform mat4 ViewMatrix;
uniform mat4 ModelMatrix;
uniform int maxLights;

in  vec3 in_Position;  // Position coming in
in  vec2 in_TexCoord;  // texture coordinate coming in
in  vec3 in_Normal;    // vertex normal used for lighting

uniform vec4 LightPos[3];  // light position

uniform float numberOfRows;
uniform vec2 texOffset1;
uniform vec2 texOffset2;
uniform float blend;

out vec2 ex_TexCoord1;  // exiting texture coord
out vec2 ex_TexCoord2;
out vec3 ex_Normal;    // exiting normal transformed by the normal matrix
out vec3 ex_PositionEye; 
out vec3 ex_LightDir[3]; 
out float ex_Blend;
out vec3 ex_ToLightVector[3];

void main(void)
{
	mat4 ModelViewMatrix = ViewMatrix * ModelMatrix;

	vec2 texCoord1 = in_TexCoord / numberOfRows + texOffset1;
	vec2 texCoord2 = in_TexCoord / numberOfRows + texOffset2;
	gl_Position = ProjectionMatrix * ModelViewMatrix * vec4(in_Position, 1.0);
	
	ex_TexCoord1 = texCoord1;
	ex_TexCoord2 = texCoord2;
		
	ex_Normal = NormalMatrix*in_Normal; 

	vec3 worldPos =  vec3((ModelMatrix * vec4(in_Position, 1.0)));

	ex_PositionEye = vec3((ModelViewMatrix * vec4(in_Position, 1.0)));

	//multiple lights
	for(int i = 0; i < maxLights; i++){
		ex_LightDir[i] = vec3(ViewMatrix * LightPos[i]);
		ex_ToLightVector[i] = LightPos[i].xyz - worldPos;
	}


	ex_Blend = blend; //used to blend between animation frames

}