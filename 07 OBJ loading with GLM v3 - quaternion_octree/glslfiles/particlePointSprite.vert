#version 150

uniform mat4 ModelViewMatrix;
uniform mat4 ProjectionMatrix;
uniform mat3 NormalMatrix;
uniform mat4 ViewMatrix;

in  vec3 in_Position;  // Position coming in
in  vec2 in_TexCoord;  // texture coordinate coming in
in  vec3 in_Normal;    // vertex normal used for lighting

uniform vec4 LightPos;  // light position

uniform float numberOfRows;
uniform vec2 texOffset1;
uniform vec2 texOffset2;
uniform float blend;

out vec2 ex_TexCoord1;  // exiting texture coord
out vec2 ex_TexCoord2;
out vec3 ex_Normal;    // exiting normal transformed by the normal matrix
out vec3 ex_PositionEye; 
out vec3 ex_LightDir; 
out float ex_Blend;

void main(void)
{
	vec2 texCoord1 = in_TexCoord / numberOfRows + texOffset1;
	vec2 texCoord2 = in_TexCoord / numberOfRows + texOffset2;
	
	//gl_PointSize = 1.0;
	gl_Position = ProjectionMatrix * ModelViewMatrix * vec4(in_Position, 1.0);
	
	ex_TexCoord1 = texCoord1;
	ex_TexCoord2 = texCoord2;
		
	ex_Normal = NormalMatrix*in_Normal; 

	ex_PositionEye = vec3((ModelViewMatrix * vec4(in_Position, 1.0)));

	ex_LightDir = vec3(ViewMatrix * LightPos);

	ex_Blend = blend;
}