#include "Wind.h"

Wind::Wind()
{
}

Wind::Wind(glm::vec3 translation, glm::vec3 rotation, glm::vec3 scale, Shader * shader, float height, glm::vec3 maxChannelVelocity, float range) : GameObject(translation, rotation, scale, shader)
{
	this->height = height;
	this->maxChannelVelocity = maxChannelVelocity;
	this->range = range;
}

float Wind::getHeight()
{
	return height;
}


glm::vec3 Wind::getMaxChannelVelocity()
{
	return maxChannelVelocity * spin; //adjust returned value based on speed of fan spin
}

float Wind::getRange()
{
	return range * spin; //adjust returned value based on speed of fan spin
}

float Wind::getSpin()
{
	return spin;
}

void Wind::setSpin(float spin)
{
	//dont let fan spin beyond a max speed
	spin = glm::clamp(spin, 0.0f, 1.0f);
	this->spin = spin;
}

void Wind::draw(glm::mat4 viewMatrix)
{
	//make the fan spin
	this->rotation.z += this->spin * rotationMultiplier;
	GameObject::draw(viewMatrix);
}

Wind::~Wind()
{
}
