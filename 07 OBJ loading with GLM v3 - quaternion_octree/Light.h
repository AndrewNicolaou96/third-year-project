#pragma once
#include "GameObject.h"
class Light
{
private:
	//multiple lights have seperate ID to be passed to shader
	int ID = 0;
	int maxLights = 2;

	//Light Properties - Multiple light source properties are contained in 2D arrays
	float Light_Ambient[2][4] = { 10.0f, 5.0f, 2.0f, 1.0f, 
								  0.0f, 50.0f, 50.0f, 1.0f };

	float Light_Diffuse[4] = { 1.8f, 1.8f, 1.6f, 1.0f };

	float Light_Specular[4] = { 10.0f,10.0f,10.0f,1.0f };

	float LightPos[2][4] = { 0.0f, -1.0f, 1.0f, 0.0f,
							 -25.0f, 40.0f, 1.0f, 0.0f };

	float attenuation[2][3] = { 1.0f, 0.001f, 0.002f, 
								1.0f, 0.032f, 0.064f };


	Shader* shader;
public:
	bool useAttenuation = true;

	Light();

	Light(Shader * shader);

	//move the second light source
	void move(float position[3]);

	//change color of the second light source
	void tint(float colour[3]);

	void update();

	~Light();
};

