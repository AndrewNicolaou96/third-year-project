#include "ParticleSystem.h"
#include <random>



ParticleSystem::ParticleSystem()
{
}

ParticleSystem::ParticleSystem(glm::vec3 origin, int maxParticles, int framesBetweenCycle, int rate, GLuint texName, float meanLifeTime, float varianceLifeTime, glm::vec3 velocityLowerBound, glm::vec3 velocityUpperBound, float spin, glm::vec3 startSize, glm::vec3 scaleFactor, Camera * camera, Shader* shader)
{
	this->origin = origin;
	this->maxParticles = maxParticles;
	this->framesbetweenCycle = framesBetweenCycle;
	this->emissionRate = rate;
	this->texName = texName;
	this->meanLifeTime = meanLifeTime;
	this->varianceLifeTime = varianceLifeTime;
	this->velocityLowerBound = velocityLowerBound;
	this->velocityUpperBound = velocityUpperBound;
	this->spin = spin;
	this->startSize = startSize;
	this->scaleFactor = scaleFactor;
	for (int i = 0; i < maxParticles; i++) {
		Particle* p = new Particle(origin, glm::vec3(0, 0, 0), startSize, shader, camera);	//construct the particle
		p->initBillboard(texName); //initialise the billboard render requirements
		initialiseParticleLife(p);
		initialiseParticlePhysics(p); //initialise the physics properties
		p->setBoundingSphere(1.0f);
		particles.push_back(p); //add the particle to the system
	}
}

glm::vec3 ParticleSystem::getOrigin()
{
	return origin;
}

std::vector<Particle*> ParticleSystem::getParticles()
{
	return particles;
}

void ParticleSystem::setOrigin(glm::vec3 origin)
{
	this->origin = origin;
}

void ParticleSystem::setAlphaCapForAllParticles(float alphaCap)
{
	for (int i = 0; i < maxParticles; i++) {
		particles[i]->setAlphaCap(alphaCap);
	}
}

void ParticleSystem::update(glm::mat4 viewingMatrix)
{
	if (this != nullptr) {
		frames++;
		//emit specific number of  particles per frameCycle
		if (frames > framesbetweenCycle) {
			activeQuantity += emissionRate;
			frames = 0;
		}
		if (activeQuantity > maxParticles) {
			activeQuantity = maxParticles;
		}
		for (int i = 0; i < activeQuantity; i++) {
			if (!particles[i]->emit) {
				particles[i]->emit = true;
				particles[i]->setTranslation(origin);
			}
		}
		//emit particles which are ready
		for (int i = 0; i < maxParticles; i++) {
			Particle* p = particles[i];
			if (p->emit) {
				p->update();
			}
		}
		killParticles();
		sortParticles();
		for (int i = 0; i < maxParticles; i++) {
			Particle* p = particles[i];
			if (p->emit) {
				p->draw(viewingMatrix);
			}
		}
	}
}

void ParticleSystem::initialiseParticlePhysics(Particle * p)
{
	//set a random velocity within the range assigned
	glm::vec3 velocity;
	velocity.x = velocityLowerBound.x + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (velocityUpperBound.x - velocityLowerBound.x)));
	velocity.y = velocityLowerBound.y + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (velocityUpperBound.y - velocityLowerBound.y)));
	velocity.z = velocityLowerBound.z + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (velocityUpperBound.z - velocityLowerBound.z)));

	p->setOriginalVelocity(velocity);

	//set a random rotation in z axis
	float zRotation = fmod(std::rand(), rotationCap);
	p->transform(origin, glm::vec3(0, 0, zRotation), startSize);

	//set the translation back to the origin;
	p->setTranslation(origin);
	p->setScale(startSize);
	
	//pass values of attributes identical for all particles
	p->setScaleFactor(scaleFactor);
	p->setSpin(spin);


	//set a random animation speed
	int minsStartFrames = 2;
	int maxStartFrames = 3;

	//animation speed is random (bounded between max and min frames)
	int frames = minsStartFrames + static_cast <float> (rand()) / (static_cast <float> (RAND_MAX / (maxStartFrames - minsStartFrames)));
	p->setFramesBetweenAnimation(frames);
}

void ParticleSystem::initialiseParticleLife(Particle * p)
{
	//random lifeTime chosen within range 
	float maxLifeTime = meanLifeTime + varianceLifeTime;
	float minLifeTime = meanLifeTime - varianceLifeTime;
	float range = maxLifeTime - minLifeTime;

	//calculate lifeTime for apecific particle
	float lifeTime = fmod(std::rand(), range) + minLifeTime;

	p->setLifeTime(lifeTime);
}

void ParticleSystem::killParticles()
{
	//search array for particles with no remaining lifetime
	for (std::vector<Particle*>::iterator it = particles.begin(); it != particles.end(); ++it) {
		Particle * p = *it;
		if (p->getRemainingLifeTime() <= 0) {
			kill(p);
		}
	}
}

void ParticleSystem::kill(Particle * p)
{
	//recycle particles
	p->resetLifeTime();
	p->emit = false;
	initialiseParticlePhysics(p);
}

void ParticleSystem::sortParticles()
{
	//sort based on distance to camera
	std::sort(&particles[0], &particles[maxParticles], ParticleSystem::compare_distance());
}

ParticleSystem::~ParticleSystem()
{
}
