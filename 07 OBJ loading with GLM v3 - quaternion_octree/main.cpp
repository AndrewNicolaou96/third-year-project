//OPENGL 3.2 DEMO FOR RENDERING OBJECTS LOADED FROM OBJ FILES

//includes areas for keyboard control, mouse control, resizing the window
//and draws a spinning rectangle

#include <windows.h>		// Header File For Windows
#include "gl/glew.h"
#include "gl/wglew.h"
#pragma comment(lib, "glew32.lib")

#include "shaders/Shader.h"   // include shader header file, this is not part of OpenGL


#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtc\type_ptr.hpp"
#include "glm\gtc\matrix_inverse.hpp"

glm::mat4 objectRotation;
glm::quat q;

Shader* particleShader;  ///shader object 
Shader* solidShader;
char* textureFileName = "TestModels/SmokeRoll.png";

//MODEL LOADING
#include "3DStruct\threeDModel.h"
#include "Obj\OBJLoader.h"
#include "GameObject.h"
#include "Light.h"
#include "Camera.h"
#include "Particle.h"
#include "ParticleSystem.h"
#include "CollisionDetector.h"
#include "Wind.h"

float amount = 0;
float temp = 0.002f;
	
//Objects
ThreeDModel model, modelbox;
OBJLoader objLoader;
Particle* cameraFocus;
ParticleSystem* smoke;
ParticleSystem* flames1;
ParticleSystem* flames2;
vector<ParticleSystem*> systems;
vector<Wind*> fans;
vector<GameObject* > colliders;
GameObject* bowl;
GameObject* bowl2;
GameObject* terrain;
GameObject* blueLightSource;
GameObject* campfire;
GameObject* rectangleCollider;
Light* particleLight;
Light* solidLight;
Camera* camera;
Wind* fan;
Wind* fan2;
GameObject* fanStand;
GameObject* fanStand2;
GLuint texName;


glm::mat4 ProjectionMatrix; // matrix for the orthographic projection

int	mouse_x=0, mouse_y=0;
bool LeftPressed = false;
int screenWidth=600, screenHeight=600;
bool keys[256];
float spin=180;
float speed=0;
bool drawBowls = false;
enum scene {RENDER_STRESS, COLLISION_STRESS, WIND_STRESS, AABB, SMOKE_COLLISION, DEFAULT};
scene currentScene = DEFAULT;

//OPENGL FUNCTION PROTOTYPES
void display();				//called in winmain to draw everything to the screen
void update();				//called in winmain to update the properties of objects
void reshape(int width, int height);				//called when the window is resized
void init();				//called in winmain when the program starts.
void processKeys();         //called in winmain to process keyboard input


/*************    START OF OPENGL FUNCTIONS   ****************/
void display()									
{

	glClear(GL_COLOR_BUFFER_BIT | GL_DEPTH_BUFFER_BIT);


	/********************************************************SET UP SHADERS**************************************************************************/
	
	//particle shader
	glUseProgram(particleShader->handle());  // use the shader

	GLuint matLocation = glGetUniformLocation(particleShader->handle(), "ProjectionMatrix");  
	glUniformMatrix4fv(matLocation, 1, GL_FALSE, &ProjectionMatrix[0][0]);

	glm::mat4 viewMatrix = camera->viewMatrix;
	glUniformMatrix4fv(glGetUniformLocation(particleShader->handle(), "ViewMatrix"), 1, GL_FALSE, &viewMatrix[0][0]);


	//solid shader
	glUseProgram(solidShader->handle());  // use the shader

	matLocation = glGetUniformLocation(solidShader->handle(), "ProjectionMatrix");
	glUniformMatrix4fv(matLocation, 1, GL_FALSE, &ProjectionMatrix[0][0]);

	glUniformMatrix4fv(glGetUniformLocation(solidShader->handle(), "ViewMatrix"), 1, GL_FALSE, &viewMatrix[0][0]);

	glUseProgram(0);

	/***********************************************************DRAW RENDERABLES*******************************************************************/


	glBindTexture(GL_TEXTURE_2D, texName);

	//Draw different scenes
	if (currentScene == RENDER_STRESS) {
		for (int i = 0; i < 125; i++){
			systems[i]->update(camera->viewMatrix);
		}
	}
	else if (currentScene == SMOKE_COLLISION) {
		flames1->update(viewMatrix);
		flames2->update(viewMatrix);
	}
	else {
		smoke->update(camera->viewMatrix);
		if (currentScene == COLLISION_STRESS) {
			if (drawBowls) {
				for (int i = 0; i < 1000; i++) {
					colliders[i]->draw(camera->viewMatrix);
				}
			}
		}
		else if (currentScene == DEFAULT){
			bowl->draw(camera->viewMatrix);
			bowl2->draw(camera->viewMatrix);
			fan->draw(camera->viewMatrix);
			fan2->draw(camera->viewMatrix);
			fanStand->draw(camera->viewMatrix);
			fanStand2->draw(camera->viewMatrix);	
		}
		else if (currentScene == AABB) {
			rectangleCollider->draw(camera->viewMatrix);
		}
		terrain->draw(camera->viewMatrix);
		blueLightSource->draw(camera->viewMatrix);
		campfire->draw(camera->viewMatrix);	
	}
	
	glFlush();
}

void update()
{
	camera->update();
	particleLight->update();
	solidLight->update();
	//perform different calculations depending on scene
	if (currentScene != RENDER_STRESS) {
		if (currentScene == COLLISION_STRESS) {
			for (int i = 0; i < 1000; i++) {
				colliders[i]->setTranslation(colliders[i]->getTranslation() + glm::vec3(0.01f,0.0f,0.0f));
				CollisionDetector::checkUniformCollisions(smoke, colliders[i], false);
			}
		}
		else if (currentScene == WIND_STRESS) {
			for (int i = 0; i < 1000; i++) {
				CollisionDetector::checkCollisionWithWindChannel(smoke, fans[i]);
			}
		}
		else if(currentScene == SMOKE_COLLISION){
			CollisionDetector::checkParticleVsParticle(flames1, flames2);
		}
		else if (currentScene == DEFAULT) {
			CollisionDetector::checkUniformCollisions(smoke, bowl, true);
			CollisionDetector::checkUniformCollisions(smoke, bowl2, true);		
			CollisionDetector::checkCollisionWithWindChannel(smoke, fan);
			CollisionDetector::checkCollisionWithWindChannel(smoke, fan2);	
		}
		else if (currentScene == AABB) {
			CollisionDetector::checkElongatedCollision(smoke, rectangleCollider, true);
		}
	}


}

void update();

void reshape(int width, int height)		// Resize the OpenGL window
{
	screenWidth=width; screenHeight = height;           // to ensure the mouse coordinates match 
														// we will use these values to set the coordinate system

	glViewport(0,0,width,height);						// Reset The Current Viewport

	//Set the projection matrix
	ProjectionMatrix = glm::perspective(60.0f, (GLfloat)screenWidth/(GLfloat)screenHeight, 1.0f, 200.0f);
}

void init()
{
	glEnable(GL_TEXTURE_2D);
	glClearColor(0.0f, 0.0f, 0.0f, 0.0f);						
	glEnable(GL_DEPTH_TEST);
	glEnable(GL_BLEND);
	glBlendFunc(GL_SRC_ALPHA, GL_ONE_MINUS_SRC_ALPHA);
	/**********************************************************TEXTURE LOADING**********************************************************************/
	// Texture loading object
	nv::Image img;

	// Return true on success
	if (img.loadImageFromFile(textureFileName))
	{
		glGenTextures(1, &texName);
		glBindTexture(GL_TEXTURE_2D, texName);
		glTexParameteri(GL_TEXTURE_2D, GL_GENERATE_MIPMAP, GL_TRUE);
		glTexImage2D(GL_TEXTURE_2D, 0, img.getInternalFormat(), img.getWidth(), img.getHeight(), 0, img.getFormat(), img.getType(), img.getLevel(0));
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MIN_FILTER, GL_LINEAR_MIPMAP_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_MAG_FILTER, GL_LINEAR);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_S, GL_CLAMP);
		glTexParameteri(GL_TEXTURE_2D, GL_TEXTURE_WRAP_T, GL_CLAMP);
		glTexParameterf(GL_TEXTURE_2D, GL_TEXTURE_MAX_ANISOTROPY_EXT, 16.0f);
		glGenerateMipmap(GL_TEXTURE_2D);
	}
	else
		MessageBox(NULL, "Failed to load texture", "End of the world", MB_OK | MB_ICONINFORMATION);


	/**********************************************************SHADER SET UP**********************************************************************/

	particleShader = new Shader;
    if(!particleShader->load("BasicView", "glslfiles/particle.vert", "glslfiles/particle.frag"))
	{
		cout << "failed to load shader" << endl;
	}		

	solidShader = new Shader;
	if (!solidShader->load("Basic", "glslfiles/basicTransformations.vert", "glslfiles/basicTransformations.frag"))
	{
		cout << "failed to load shader" << endl;
	}

	/**********************************************************GameObject Initialisation***********************************************************/
	

	//Default transformations
	glm::vec3 translation = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 rotation = glm::vec3(0.0f, 0.0f, 0.0f);
	glm::vec3 scale = glm::vec3(1.0f, 1.0f, 1.0f);

	//uppper and lowerbounds of smoke velocity;
	glm::vec3 velocityLowerBound = glm::vec3(-0.005, 0.03, 0); 
	glm::vec3 velocityUpperBound = glm::vec3(0.005, 0.035, 0);
	
	float spin = 0.0f;
	//small growth in all axes for smoke particles
	glm::vec3 scaleFactor = glm::vec3(0.01f, 0.01f, 0.01f);

	glm::vec3 startSize = glm::vec3(1.0f, 1.0f, 1.0f);


	//construction of GameObjects
	camera = new Camera(translation, rotation, scale, particleShader);
	cameraFocus = new Particle(translation, rotation, scale, particleShader, camera);
	bowl = new GameObject(translation, rotation, scale, solidShader);
	bowl2 = new GameObject(translation, rotation, scale, solidShader);
	terrain = new GameObject(translation, rotation, scale, solidShader);
	fan = new Wind(translation, rotation, scale, solidShader, 5.0f, glm::vec3(-0.03f, 0.0f, 0.0f), 20.0f);
	fan2 = new Wind(translation, rotation, scale, solidShader, 5.0f, glm::vec3(0.03f, 0.0f, 0.0f), 20.0f);
	fanStand = new GameObject(translation, rotation, scale, solidShader);
	fanStand2 = new GameObject(translation, rotation, scale, solidShader);
	blueLightSource = new GameObject(translation, rotation, scale, solidShader);
	campfire = new GameObject(translation, rotation, scale, solidShader);
	rectangleCollider = new GameObject(translation, rotation, scale, solidShader);

	//create colliders for collision detection stress test
	for (int i = 0; i < 1000; i++) {
		colliders.push_back(new GameObject(translation, rotation, scale, solidShader));
	}

	//create cfans for wind channel stress test
	for (int i = 0; i < 1000; i++) {
		fans.push_back(new Wind(translation, rotation, scale, solidShader, 5.0f, glm::vec3(-0.03f, 0.0f, 0.0f), 20.0f));
	}
	
	startSize = glm::vec3(2.0f, 2.0f, 2.0f);

	//particles with collision
	smoke = new ParticleSystem(translation, 300, 20, 2, texName, 3000, 150, velocityLowerBound, velocityUpperBound, spin, startSize, scaleFactor, camera, particleShader);
	smoke->setAlphaCapForAllParticles(0.4f);

	velocityLowerBound = glm::vec3(0.050f, -0.005, 0);
	velocityUpperBound = glm::vec3(0.055f, 0.005, 0);

	flames1 = new ParticleSystem(translation, 300, 20, 2, texName, 1500, 150, velocityLowerBound, velocityUpperBound, spin, startSize, scaleFactor, camera, particleShader);
	flames1->setAlphaCapForAllParticles(0.4f);

	velocityLowerBound = glm::vec3(-0.050f, -0.005, 0);
	velocityUpperBound = glm::vec3(-0.055f, 0.005, 0);

	flames2 = new ParticleSystem(translation, 300, 20, 2, texName, 1500, 150, velocityLowerBound, velocityUpperBound, spin, startSize, scaleFactor, camera, particleShader);
	flames2->setAlphaCapForAllParticles(0.4f);

	startSize = glm::vec3(2.0f, 2.0f, 2.0f);
	scaleFactor = glm::vec3(0.01f, 0.01f, 0.01f);
	velocityLowerBound = glm::vec3(0, 0.03, 0);
	velocityUpperBound = glm::vec3(0, 0.035, 0);
	

	//particles without collision - for draw call stress test
	for (int i = 0; i < 125; i++) {
		systems.push_back(new ParticleSystem(translation, 30, 30, 1, texName, 1000, 150, velocityLowerBound, velocityUpperBound, spin, startSize, scaleFactor, camera, particleShader));
	}

	//initiate lighting
	particleLight = new Light(particleShader);
	solidLight = new Light(solidShader);



	//initialisation of renderables
	fan->initOBJ(objLoader, "TestModels/fan.obj");
	fan2->initOBJ(objLoader, "TestModels/fan.obj");
	fanStand->initOBJ(objLoader, "TestModels/fanStand.obj");
	fanStand2->initOBJ(objLoader, "TestModels/fanStand.obj");
	terrain->initOBJ(objLoader, "TestModels/basicTerrain.obj");
	blueLightSource->initOBJ(objLoader, "TestModels/sphere.obj");
	campfire->initOBJ(objLoader, "TestModels/campFire.obj");
	bowl->initOBJ(objLoader, "TestModels/bowl.obj");
	bowl2->initOBJ(objLoader, "TestModels/bowl.obj");
	rectangleCollider->initOBJ(objLoader, "TestModels/rectangle.obj");

	//initiate collider objs
	for (int i = 0; i < 1000; i++) {
		colliders[i]->initOBJ(objLoader, "TestModels/bowl.obj");
	}

	//Tranformation of renderables
	terrain->transform(glm::vec3(0.0f, -3.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f));
	campfire->transform(glm::vec3(0.0f, -3.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(3.0f, 3.0f, 3.0f));
	fan->transform(glm::vec3(8.0f, 25.0f, 0.0f), glm::vec3(0.0f, 90.0f, 0.0f), glm::vec3(3.0f, 3.0f, 3.0f));
	fan2->transform(glm::vec3(-20.0f, 35.0f, 0.0f), glm::vec3(0.0f, -90.0f, 0.0f), glm::vec3(3.0f, 3.0f, 3.0f));
	fanStand->transform(glm::vec3(8.0f, 25.0f, 0.0f), glm::vec3(0.0f, 90.0f, 0.0f), glm::vec3(3.0f, 3.0f, 3.0f));
	fanStand2->transform(glm::vec3(-20.0f, 35.0f, 0.0f), glm::vec3(0.0f, -90.0f, 0.0f), glm::vec3(3.0f, 3.0f, 3.0f));
	camera->transform(glm::vec3(0.0f, 50.0f, 75.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f));
	cameraFocus->transform(glm::vec3(0.0f, 50.0f, 75.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f));
	bowl->transform(glm::vec3(0.0f, 5.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(3.0f, 3.0f, 3.0f));
	rectangleCollider->transform(glm::vec3(0.0f, 15.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(3.0f, 3.0f, 3.0f));
	bowl2->transform(glm::vec3(-2.0f, 15.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(3.0f, 3.0f, 3.0f));
	blueLightSource->transform(glm::vec3(-25.0f, 40.0f, 1.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f));

	flames1->setOrigin(glm::vec3(-40.0f, 5.0f, 0.0f));
	flames2->setOrigin(glm::vec3(40.0f, 5.0f, 0.0f));


	
	//transform particle systems into 5x5x5 grid
	for (int x = 0; x < 5; x++) {
		for (int y = 0; y < 5; y++) {
			for (int z = 0; z < 5; z++) {
				systems[x+y*5+z*25]->setOrigin(glm::vec3(20.0f * x - 45.0f, 20.0f * y -25.0f, 20.0f * z - 100.0f));
			}
		}
	}

	//transform colliders into grid
	for (int x = 0; x < 50; x++) {
		for (int y = 0; y < 20; y++) {
			colliders[x + y * 50]->transform(glm::vec3(x* 8.0f - 400.0f, y * 8.0f + 10.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(3.0f, 3.0f, 3.0f));
		}
	}

	//transform fans into grid
	for (int x = 0; x < 50; x++) {
		for (int y = 0; y < 20; y++) {
			fans[x + y * 50]->transform(glm::vec3(x* 4.0f - 100.0f, y * 4.0f - 47.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(3.0f, 3.0f, 3.0f));
			fans[x + y * 50]->setSpin(1.0f);
		}
	}
	//always focus camera in front
	camera->setFocus(cameraFocus);
}
void processKeys()
{
	/*****************************************SCENE SELECTION*****************************************/
	if (keys['1']) {
		currentScene = DEFAULT;
		particleLight->useAttenuation = true;
		solidLight->useAttenuation = true;
	}
	if (keys['2']) {
		currentScene = RENDER_STRESS;
		particleLight->useAttenuation = false;
		solidLight->useAttenuation = false;
	}
	if (keys['3']) {
		currentScene = COLLISION_STRESS;
		particleLight->useAttenuation = true;
		solidLight->useAttenuation = true;
		drawBowls = true;
	}
	if (keys['B']) {
		drawBowls = false;
	}
	if (keys['4']) {
		currentScene = WIND_STRESS;
		particleLight->useAttenuation = true;
		solidLight->useAttenuation = true;
	}
	if (keys['5']) {
		currentScene = SMOKE_COLLISION;
		particleLight->useAttenuation = true;
		solidLight->useAttenuation = true;
	}
	if (keys['6']) {
		currentScene = AABB;
		particleLight->useAttenuation = true;
		solidLight->useAttenuation = true;
	}
	/**********************************************CONTROLS***********************************************/
	//move the particle system or light source
	if (keys[VK_LEFT]) {
		if (keys[VK_SHIFT]) {
			blueLightSource->transform(glm::vec3(-0.1f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f));
			float coordinates[3] = { -0.1f, 0.0f, 0.0f };
			particleLight->move(coordinates);
			solidLight->move(coordinates);
		}
		else {
			smoke->setOrigin(glm::vec3(smoke->getOrigin().x - 0.1f, smoke->getOrigin().y, smoke->getOrigin().z));
		}
	}
	if (keys[VK_RIGHT]) {
		if (keys[VK_SHIFT]) {
			blueLightSource->transform(glm::vec3(0.1f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f));
			float coordinates[3] = { 0.1f, 0.0f, 0.0f };
			particleLight->move(coordinates);
			solidLight->move(coordinates);
		}
		else {
			smoke->setOrigin(glm::vec3(smoke->getOrigin().x + 0.1f, smoke->getOrigin().y, smoke->getOrigin().z));
		}
	}
	//change light tint
	if (keys['8']) {
		float color[3] = { 1.0f, 0.0f, -1.0f };
		particleLight->tint(color);
		solidLight->tint(color);
	}
	if (keys['7']) {
		float color[3] = { -1.0f, 0.0f, 1.0f };
		particleLight->tint(color);
		solidLight->tint(color);
	}
	if(keys['I'])
	{
		//translate camera up
		cameraFocus->transform(glm::vec3(0.0f, 0.1f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f));
	}
	if (keys['K'])
	{
		//translate camera down
		cameraFocus->transform(glm::vec3(0.0f, -0.1f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f));
	}
	if (keys['J'])
	{
		//translate camera left
		cameraFocus->transform(glm::vec3(-0.1f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f));
	}
	if (keys['L'])
	{
		//translate camera right
		cameraFocus->transform(glm::vec3(0.1f, 0.0f, 0.0f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f));
	}
	if (keys['W'])
	{
		//translate camera forward
		cameraFocus->transform(glm::vec3(0.0f, 0.0f, -0.1f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f));
	}
	if (keys['S'])
	{
		//translate camera backward
		cameraFocus->transform(glm::vec3(0.0f, 0.0f, 0.1f), glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f));
	}
	if (keys['A'])
	{
		//rotate camera clockwise in y axis
		cameraFocus->transform(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, 0.1f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f));
	}
	if (keys['D'])
	{
		//rotate camera anti-clockwise in y axis
		cameraFocus->transform(glm::vec3(0.0f, 0.0f, 0.0f), glm::vec3(0.0f, -0.1f, 0.0f), glm::vec3(1.0f, 1.0f, 1.0f));
	}
	if (keys['0'])
	{
		//increase spin
		fan->setSpin(fan->getSpin() + 0.005f);
		fan2->setSpin(fan->getSpin() + 0.005f);
	}
	if (keys['9'])
	{
		//decrease spin
		fan->setSpin(fan->getSpin() - 0.005f);
		fan2->setSpin(fan->getSpin() - 0.005f);
	}
}

/**************** END OPENGL FUNCTIONS *************************/

//WIN32 functions
LRESULT	CALLBACK WndProc(HWND, UINT, WPARAM, LPARAM);	// Declaration For WndProc
void KillGLWindow();									// releases and destroys the window
bool CreateGLWindow(char* title, int width, int height); //creates the window
int WINAPI WinMain(	HINSTANCE, HINSTANCE, LPSTR, int);  // Win32 main function

//win32 global variabless
HDC			hDC=NULL;		// Private GDI Device Context
HGLRC		hRC=NULL;		// Permanent Rendering Context
HWND		hWnd=NULL;		// Holds Our Window Handle
HINSTANCE	hInstance;		// Holds The Instance Of The Application


/******************* WIN32 FUNCTIONS ***************************/
int WINAPI WinMain(	HINSTANCE	hInstance,			// Instance
					HINSTANCE	hPrevInstance,		// Previous Instance
					LPSTR		lpCmdLine,			// Command Line Parameters
					int			nCmdShow)			// Window Show State
{
	MSG		msg;									// Windows Message Structure
	bool	done=false;								// Bool Variable To Exit Loop

	AllocConsole();
	FILE* stream;
	freopen_s(&stream, "CONOUT$", "w", stdout);

	// Create Our OpenGL Window
	if (!CreateGLWindow("OpenGL Win32 Example",screenWidth,screenHeight))
	{
		return 0;									// Quit If Window Was Not Created
	}

	while(!done)									// Loop That Runs While done=FALSE
	{
		if (PeekMessage(&msg,NULL,0,0,PM_REMOVE))	// Is There A Message Waiting?
		{
			if (msg.message==WM_QUIT)				// Have We Received A Quit Message?
			{
				done=true;							// If So done=TRUE
			}
			else									// If Not, Deal With Window Messages
			{
				TranslateMessage(&msg);				// Translate The Message
				DispatchMessage(&msg);				// Dispatch The Message
			}
		}
		else										// If There Are No Messages
		{
			if(keys[VK_ESCAPE])
				done = true;

			processKeys();			//process keyboard
			update();				//update object properties
			display();					// Draw The Scene
			SwapBuffers(hDC);				// Swap Buffers (Double Buffering)
		}
	}

	
	// Shutdown
	KillGLWindow();									// Kill The Window
	return (int)(msg.wParam);						// Exit The Program
}

//WIN32 Processes function - useful for responding to user inputs or other events.
LRESULT CALLBACK WndProc(	HWND	hWnd,			// Handle For This Window
							UINT	uMsg,			// Message For This Window
							WPARAM	wParam,			// Additional Message Information
							LPARAM	lParam)			// Additional Message Information
{
	switch (uMsg)									// Check For Windows Messages
	{
		case WM_CLOSE:								// Did We Receive A Close Message?
		{
			PostQuitMessage(0);						// Send A Quit Message
			return 0;								// Jump Back
		}
		break;

		case WM_SIZE:								// Resize The OpenGL Window
		{
			reshape(LOWORD(lParam),HIWORD(lParam));  // LoWord=Width, HiWord=Height
			return 0;								// Jump Back
		}
		break;

		case WM_LBUTTONDOWN:
			{
	            mouse_x = LOWORD(lParam);          
				mouse_y = screenHeight - HIWORD(lParam);
				LeftPressed = true;
			}
		break;

		case WM_LBUTTONUP:
			{
	            LeftPressed = false;
			}
		break;

		case WM_MOUSEMOVE:
			{
	            mouse_x = LOWORD(lParam);          
				mouse_y = screenHeight  - HIWORD(lParam);
			}
		break;
		case WM_KEYDOWN:							// Is A Key Being Held Down?
		{
			keys[wParam] = true;					// If So, Mark It As TRUE
			return 0;								// Jump Back
		}
		break;
		case WM_KEYUP:								// Has A Key Been Released?
		{
			keys[wParam] = false;					// If So, Mark It As FALSE
			return 0;								// Jump Back
		}
		break;
	}

	// Pass All Unhandled Messages To DefWindowProc
	return DefWindowProc(hWnd,uMsg,wParam,lParam);
}

void KillGLWindow()								// Properly Kill The Window
{
	if (hRC)											// Do We Have A Rendering Context?
	{
		if (!wglMakeCurrent(NULL,NULL))					// Are We Able To Release The DC And RC Contexts?
		{
			MessageBox(NULL,"Release Of DC And RC Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}

		if (!wglDeleteContext(hRC))						// Are We Able To Delete The RC?
		{
			MessageBox(NULL,"Release Rendering Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		}
		hRC=NULL;										// Set RC To NULL
	}

	if (hDC && !ReleaseDC(hWnd,hDC))					// Are We Able To Release The DC
	{
		MessageBox(NULL,"Release Device Context Failed.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hDC=NULL;										// Set DC To NULL
	}

	if (hWnd && !DestroyWindow(hWnd))					// Are We Able To Destroy The Window?
	{
		MessageBox(NULL,"Could Not Release hWnd.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hWnd=NULL;										// Set hWnd To NULL
	}

	if (!UnregisterClass("OpenGL",hInstance))			// Are We Able To Unregister Class
	{
		MessageBox(NULL,"Could Not Unregister Class.","SHUTDOWN ERROR",MB_OK | MB_ICONINFORMATION);
		hInstance=NULL;									// Set hInstance To NULL
	}
}

/*	This Code Creates Our OpenGL Window.  Parameters Are:					*
 *	title			- Title To Appear At The Top Of The Window				*
 *	width			- Width Of The GL Window Or Fullscreen Mode				*
 *	height			- Height Of The GL Window Or Fullscreen Mode			*/
 
bool CreateGLWindow(char* title, int width, int height)
{
	GLuint		PixelFormat;			// Holds The Results After Searching For A Match
	WNDCLASS	wc;						// Windows Class Structure
	DWORD		dwExStyle;				// Window Extended Style
	DWORD		dwStyle;				// Window Style
	RECT		WindowRect;				// Grabs Rectangle Upper Left / Lower Right Values
	WindowRect.left=(long)0;			// Set Left Value To 0
	WindowRect.right=(long)width;		// Set Right Value To Requested Width
	WindowRect.top=(long)0;				// Set Top Value To 0
	WindowRect.bottom=(long)height;		// Set Bottom Value To Requested Height

	hInstance			= GetModuleHandle(NULL);				// Grab An Instance For Our Window
	wc.style			= CS_HREDRAW | CS_VREDRAW | CS_OWNDC;	// Redraw On Size, And Own DC For Window.
	wc.lpfnWndProc		= (WNDPROC) WndProc;					// WndProc Handles Messages
	wc.cbClsExtra		= 0;									// No Extra Window Data
	wc.cbWndExtra		= 0;									// No Extra Window Data
	wc.hInstance		= hInstance;							// Set The Instance
	wc.hIcon			= LoadIcon(NULL, IDI_WINLOGO);			// Load The Default Icon
	wc.hCursor			= LoadCursor(NULL, IDC_ARROW);			// Load The Arrow Pointer
	wc.hbrBackground	= NULL;									// No Background Required For GL
	wc.lpszMenuName		= NULL;									// We Don't Want A Menu
	wc.lpszClassName	= "OpenGL";								// Set The Class Name

	if (!RegisterClass(&wc))									// Attempt To Register The Window Class
	{
		MessageBox(NULL,"Failed To Register The Window Class.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;											// Return FALSE
	}
	
	dwExStyle=WS_EX_APPWINDOW | WS_EX_WINDOWEDGE;			// Window Extended Style
	dwStyle=WS_OVERLAPPEDWINDOW;							// Windows Style
	
	AdjustWindowRectEx(&WindowRect, dwStyle, FALSE, dwExStyle);		// Adjust Window To True Requested Size

	// Create The Window
	if (!(hWnd=CreateWindowEx(	dwExStyle,							// Extended Style For The Window
								"OpenGL",							// Class Name
								title,								// Window Title
								dwStyle |							// Defined Window Style
								WS_CLIPSIBLINGS |					// Required Window Style
								WS_CLIPCHILDREN,					// Required Window Style
								0, 0,								// Window Position
								WindowRect.right-WindowRect.left,	// Calculate Window Width
								WindowRect.bottom-WindowRect.top,	// Calculate Window Height
								NULL,								// No Parent Window
								NULL,								// No Menu
								hInstance,							// Instance
								NULL)))								// Dont Pass Anything To WM_CREATE
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Window Creation Error.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	static	PIXELFORMATDESCRIPTOR pfd=				// pfd Tells Windows How We Want Things To Be
	{
		sizeof(PIXELFORMATDESCRIPTOR),				// Size Of This Pixel Format Descriptor
		1,											// Version Number
		PFD_DRAW_TO_WINDOW |						// Format Must Support Window
		PFD_SUPPORT_OPENGL |						// Format Must Support OpenGL
		PFD_DOUBLEBUFFER,							// Must Support Double Buffering
		PFD_TYPE_RGBA,								// Request An RGBA Format
		24,										// Select Our Color Depth
		0, 0, 0, 0, 0, 0,							// Color Bits Ignored
		0,											// No Alpha Buffer
		0,											// Shift Bit Ignored
		0,											// No Accumulation Buffer
		0, 0, 0, 0,									// Accumulation Bits Ignored
		24,											// 24Bit Z-Buffer (Depth Buffer)  
		0,											// No Stencil Buffer
		0,											// No Auxiliary Buffer
		PFD_MAIN_PLANE,								// Main Drawing Layer
		0,											// Reserved
		0, 0, 0										// Layer Masks Ignored
	};
	
	if (!(hDC=GetDC(hWnd)))							// Did We Get A Device Context?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Create A GL Device Context.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if (!(PixelFormat=ChoosePixelFormat(hDC,&pfd)))	// Did Windows Find A Matching Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Find A Suitable PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	if(!SetPixelFormat(hDC,PixelFormat,&pfd))		// Are We Able To Set The Pixel Format?
	{
		KillGLWindow();								// Reset The Display
		MessageBox(NULL,"Can't Set The PixelFormat.","ERROR",MB_OK|MB_ICONEXCLAMATION);
		return false;								// Return FALSE
	}

	HGLRC tempContext = wglCreateContext(hDC);
	wglMakeCurrent(hDC, tempContext);

	glewInit();

	int attribs[] =
	{
		WGL_CONTEXT_MAJOR_VERSION_ARB, 3,
		WGL_CONTEXT_MINOR_VERSION_ARB, 1,
		WGL_CONTEXT_FLAGS_ARB, WGL_CONTEXT_FORWARD_COMPATIBLE_BIT_ARB,
		0
	};
	
    if(wglewIsSupported("WGL_ARB_create_context") == 1)
    {
		hRC = wglCreateContextAttribsARB(hDC,0, attribs);
		wglMakeCurrent(NULL,NULL);
		wglDeleteContext(tempContext);
		wglMakeCurrent(hDC, hRC);
	}
	else
	{	//It's not possible to make a GL 3.x context. Use the old style context (GL 2.1 and before)
		hRC = tempContext;
		cout << " not possible to make context "<< endl;
	}

	//Checking GL version
	const GLubyte *GLVersionString = glGetString(GL_VERSION);

	cout << GLVersionString << endl;

	//OpenGL 3.2 way of checking the version
	int OpenGLVersion[2];
	glGetIntegerv(GL_MAJOR_VERSION, &OpenGLVersion[0]);
	glGetIntegerv(GL_MINOR_VERSION, &OpenGLVersion[1]);

	cout << OpenGLVersion[0] << " " << OpenGLVersion[1] << endl;

	ShowWindow(hWnd,SW_SHOW);						// Show The Window
	SetForegroundWindow(hWnd);						// Slightly Higher Priority
	SetFocus(hWnd);									// Sets Keyboard Focus To The Window
	reshape(width, height);					// Set Up Our Perspective GL Screen

	init();
	
	return true;									// Success
}



