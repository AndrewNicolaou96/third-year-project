#pragma once
#include "Particle.h"
/*Object representing the particle system. the container and initialiser of particle entities*/
class ParticleSystem
{
private:
	/*******************************************FIELDS*****************************************/
	//system properties
	glm::vec3 origin;
	int maxParticles;
	int framesbetweenCycle; //number of frames to wait until the next cycle
	int emissionRate; //number of particles to emit per cycle;
	int frames; //frame counter between cycles
	int activeQuantity = 0; //number of active particles
	std::vector<Particle*> particles;	//the particle entities in the systme
	GLuint texName;	//name of texture in GPU memory
	Shader* shader;
	int rotationCap = 30; //max rotation a particle can have in the z axis to start
	
	//per-particle properties
	float meanLifeTime;	//average lifetime of a standard particle
	float varianceLifeTime; //the variance potential between particle lifetimes
	glm::vec3 velocityLowerBound;	//maximum possible start velocity of a particle
	glm::vec3 velocityUpperBound;	//maximum possible end velocity of a particle
	float spin;	//the amount to rotate a particle by oer frame
	glm::vec3 startSize;	//start size of a particle
	glm::vec3 scaleFactor;	//the amount a particle grows by per frame (same as GameObject scaleFactor)

	/***************************************FUNCTIONS******************************************/

	//used on particle birth/construction to set attributes
	void initialiseParticlePhysics(Particle *p);
	void initialiseParticleLife(Particle* p);

	//recycles particles with lifeTime below 0
	void killParticles();
	void kill(Particle* p);

	//sorts the particles by depth for correct alpha blending
	void sortParticles();
public:
	//constructors
	ParticleSystem();
	ParticleSystem(glm::vec3 origin, int maxParticles, int framesBetweenCycle, int rate, GLuint texName, float meanLifeTime, float varianceLifeTime, glm::vec3 velocityLowerBound, glm::vec3 velocityUpperBound, float spin, glm::vec3 startSize, glm::vec3 scaleFactor, Camera* camera, Shader* shader);


	//ACCESSORS/MUTATORS
	glm::vec3 getOrigin();
	std::vector<Particle*> getParticles();

	void setOrigin(glm::vec3 origin);
	void setAlphaCapForAllParticles(float alphaCap); //only call on init to overried alpha cap - do not call on update

	//draws particles and updates their position
	void update(glm::mat4 viewingMatrix);

	//used to sort particles by distance from the camera
	struct compare_distance {
		inline bool operator() (Particle* p1, Particle* p2)
		{
			return (p1->getCameraDistance() > p2->getCameraDistance());
		}
	};

	~ParticleSystem();
};

