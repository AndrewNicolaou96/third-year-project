#pragma once
#include "GameObject.h"
#include "ParticleSystem.h"
#include "Octree\Octree.h"
#include "3dStruct\threeDModel.h"
#include "Wind.h"
#define MAXDISTANCE 60.0f
/*a staic class used to detect collisions between gameObjetcs*/
class CollisionDetector
{
private:
	/*****************************DETECTION***********************************/

	//checks collision using bounding sphere algorithm
	static bool boundingSphere(Particle * p, GameObject* g);

	//checks collision using a bounding capsule algorithm - faster than aabb
	static bool boundingBox(Particle * p, GameObject* g);

	/******************************REACTION************************************/

	//how a particle should react when colliding with a solid object
	//reactZaxis determines if the smoke will move in z axis after collision
	static void particleVsSolidReaction(Particle * p, GameObject * g, bool reactZaxis);

	//how a particle should react when colliding with another particle (fluid object)
	static void particleVsparticleReaction(Particle * p1, Particle * p2);

	//applies forces to particles in a wind channel
	static void reactToWindChannel(Particle* p, Wind* windSource);
public:
	//resolves collisions between particles and dynamic objects - uses bounding sphere
	//reactZaxis determines if the smoke will move in z axis after collision
	static void checkUniformCollisions(ParticleSystem* particleSystem, GameObject* g, bool reactZaxis);
	
	//resolves collision between particles and dynamic objects - uses capsule based colliders
	//better for elongated objects (rectangles as opposed to squard
	//reactZaxis determines if the smoke will move in z axis after collision
	static void checkElongatedCollision(ParticleSystem* particleSystem, GameObject* g, bool reactZaxis);

	//passing eligible particles to be checked for collision with wind channel
	static void checkCollisionWithWindChannel(ParticleSystem* particleSystem, Wind* windSource); //CALL AFTER CHECKCOLLISIONS

	//checks collision between two particle systems
	static void checkParticleVsParticle(ParticleSystem* system1, ParticleSystem* system2);

};

