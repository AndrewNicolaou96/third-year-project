#pragma once
#define valueOFPi 3.1415926535897932384626433832795028841971693993751058209749445923078164062
#include <windows.h>		// Header File For Windows
#include "gl/glew.h"
#include "gl/wglew.h"
#pragma comment(lib, "glew32.lib")

#include "shaders/Shader.h"   // include shader header file, this is not part of OpenGL


#include "glm\glm.hpp"
#include "glm\gtc\matrix_transform.hpp"
#include "glm\gtc\type_ptr.hpp"
#include "glm\gtc\matrix_inverse.hpp"

//MODEL LOADING
#include "3DStruct\threeDModel.h"
#include "Obj\OBJLoader.h"
#include "Images/nvImage.h"
/*base class of all objects in the engine - use to define a visible object in worldspace*/
class GameObject
{
private:
	//assets
	ThreeDModel model;

protected:
	Shader* shader;

	//Material properties
	float Material_Ambient[4] = { 0.1f, 0.1f, 0.1f, 1.0f };
	float Material_Diffuse[4] = { 0.3f, 0.3f, 0.3f, 1.0f };
	float Material_Specular[4] = { 0.1f,0.1f,0.1f,1.0f };
	float Material_Shininess = 50;
	float Material_Alpha = 1.0f;

	//Transform properties
	glm::vec3 translation;
	glm::vec3 rotation;
	glm::vec3 scale;
	glm::mat4 modelMatrix;

	//size of radius of bounding sphere for collision detection
	float boundingSphereRadius = 0;

public:
	//constructors
	GameObject();
	GameObject(glm::vec3 translation, glm::vec3 rotation, glm::vec3 scale, Shader * shader);

	//ACCESSORS / MUTATORS
	glm::vec3 getTranslation();
	glm::vec3 getRotation();
	glm::vec3 getScale();
	float getBoundingSphere();
	ThreeDModel getModel();

	void setTranslation(glm::vec3 translation);
	void setBoundingSphere(float radius);
	void setAlpha(float alpha);

	//manipulate the object in world space
	void transform(glm::vec3 translation, glm::vec3 rotation, glm::vec3 scale);

	//calculate bounding sphere based on bounding box size;
	void calculateBoundingSphere();

	//initialise a model
	void initOBJ(OBJLoader objLoader, char * modelName);

	//draw the object
	void draw(glm::mat4 viewMatrix);

	//destructor
	~GameObject();
};

