#pragma once
#include "GameObject.h"
/*Object which represents the camera - used to contain the vieMatrix and perform necessary tranformations to move the camera and focus on objects*/
class Camera :
	public GameObject
{
private:
	GameObject* focus;
public:
	glm::mat4 viewMatrix = glm::mat4(1.0f);
	
	Camera();
	Camera(glm::vec3 translation, glm::vec3 rotation, glm::vec3 scale, Shader * shader);

	void setFocus(GameObject * focus);

	void update();

	~Camera();
};

