#include "CollisionDetector.h"

bool CollisionDetector::boundingSphere(Particle* p, GameObject* g)
{
	//simple distance vs radii check
	if (glm::distance(p->getTranslation(), g->getTranslation()) < p->getDimensions() * p->getScale().y + g->getBoundingSphere()) {
		return true;
	}
	return false;
}

bool CollisionDetector::boundingBox(Particle * p, GameObject* g)
{
	//initial sphere check 
	if (boundingSphere(p, g)) {
		//bounding box check AABB
		if (p->getTranslation().x + p->getDimensions() * p->getScale().x >= g->getModel().theBBox.left() + g->getTranslation().x && p->getTranslation().x - p->getDimensions() * p->getScale().x <= g->getModel().theBBox.right() + g->getTranslation().x &&
			p->getTranslation().y + p->getDimensions() * p->getScale().y >= g->getModel().theBBox.bottom() + g->getTranslation().y && p->getTranslation().y - p->getDimensions() * p->getScale().y <= g->getModel().theBBox.top() * 2 + g->getTranslation().y &&
			p->getTranslation().z - p->getDimensions() * p->getScale().z <= g->getModel().theBBox.back() + g->getTranslation().z && p->getTranslation().z + p->getDimensions() * p->getScale().z >= g->getModel().theBBox.front() + g->getTranslation().z) {
			return true;
		}
	}
	return false;
}

void CollisionDetector::reactToWindChannel(Particle * p, Wind * windSource)
{
	//distance culling for performance
	float distance = glm::distance(p->getTranslation(), windSource->getTranslation());
	if (distance < windSource->getRange()) {
		//if particle is within influence of windchannel
		if (p->getTranslation().y < windSource->getTranslation().y + windSource->getHeight() &&
			p->getTranslation().y > windSource->getTranslation().y - windSource->getHeight()) {
			//apply a force to particle which is inversely proportional to distance from the wind source
			float distanceMultiplier = windSource->getRange() - distance;
			distanceMultiplier = glm::clamp(distanceMultiplier, 0.0f, windSource->getRange());
			glm::vec3 appliedVelocity = windSource->getMaxChannelVelocity() / distanceMultiplier;
			p->setTranslation(p->getTranslation() + windSource->getMaxChannelVelocity());
		}
	}
}

void CollisionDetector::particleVsSolidReaction(Particle * p , GameObject * g, bool reactZaxis)
{
	//set collision flag to true so that "non-collisions" with other objects done override response.
	//collision flag is reset to false at the end of the particle update function on this frame
	p->colliding = true;
	//set y velocity to 0
	p->velocity.y = 0;
	p->setScaleFactor(glm::vec3(-0.05f, -0.05f, -0.05f));
	//determine x and z velocity based on position of particles
	if (p->getTranslation().x >= g->getTranslation().x) {
		//go left
		p->velocity.x = p->getOriginalVelocity().y;
		p->setSpin(100.0f);
	}
	else {
		// go right
		p->velocity.x = -p->getOriginalVelocity().y;
		p->setSpin(-100.0f);
	}
	//certain forms of reaction look best when only happening in one axis - specifically the render stress scene
	if (reactZaxis) {
		if (p->getTranslation().z >= g->getTranslation().z) {
			//go forward
			p->velocity.z = p->getOriginalVelocity().y;
		}
		else {
			//go right
			p->velocity.z = -p->getOriginalVelocity().y;
		}
	}
}

void CollisionDetector::particleVsparticleReaction(Particle * p1, Particle * p2)
{
	//set collision flag to true so that "non-collisions" with other objects done override response.
	//collision flag is reset to false at the end of the particle update function on this frame
	p1->colliding = true;
	p2->colliding = true;
	//set y velocity to 0
	p1->velocity = glm::vec3(0);
	p2->velocity = glm::vec3(0);
	//limit growth so plume does not get too big
	p1->setMaxScale(glm::vec3(50.0f));
	p2->setMaxScale(glm::vec3(50.0f));
	//determine x and z velocity based on position of particles
	//this code is vital to the "plume" effect 
	if (p1->getTranslation().y >= p2->getTranslation().y) {
		p1->velocity.y = p1->getOriginalVelocity().x / 2;
		p2->velocity.y = -p2->getOriginalVelocity().x / 2;
		p1->setSpin(100.0f);
		p2->setSpin(100.0f);

	}
	else {
		p1->velocity.y = -p1->getOriginalVelocity().x / 2;
		p2->velocity.y = p2->getOriginalVelocity().x / 2;
		p1->setSpin(-100.0f);
		p2->setSpin(-100.0f);
	}
}

void CollisionDetector::checkUniformCollisions(ParticleSystem* particleSystem, GameObject* g, bool reactZaxis)
{
	//initial distance culling
	if (glm::distance(particleSystem->getOrigin(), g->getTranslation()) < MAXDISTANCE) {
		std::vector<Particle* > particles = particleSystem->getParticles();
		for (std::vector<Particle*>::iterator it = particles.begin(); it != particles.end(); ++it) {
			Particle * p = *it;
			//collision check
			if (boundingSphere(p, g)) {
				//collision action
				particleVsSolidReaction(p, g, reactZaxis);
			}
			else {
				//action if particle is not colliding with anything (restore default values)
				if (!p->colliding) {
					p->setVelocity(p->getOriginalVelocity());
					p->setScaleFactor(glm::vec3(0.01f, 0.01f, 0.01f));
					p->setSpin(-0.0f);
				}
			}
		}
	}
}


void CollisionDetector::checkElongatedCollision(ParticleSystem * particleSystem, GameObject * g, bool reactZaxis)
{
	//initial distance culling
	if (glm::distance(particleSystem->getOrigin(), g->getTranslation()) < MAXDISTANCE) {
		std::vector<Particle* > particles = particleSystem->getParticles();
		for (std::vector<Particle*>::iterator it = particles.begin(); it != particles.end(); ++it) {
			Particle * p = *it;
			//collision check
			if (boundingBox(p, g)) {
				//collision action
				particleVsSolidReaction(p, g, reactZaxis);
			}
			else {
				//action if particle is not colliding with anything (restore default values)
				if (!p->colliding) {
					p->setVelocity(p->getOriginalVelocity());
					p->setScaleFactor(glm::vec3(0.01f, 0.01f, 0.01f));
					p->setSpin(-0.0f);
				}
			}
		}
	}
}

void CollisionDetector::checkCollisionWithWindChannel(ParticleSystem * particleSystem, Wind* windSource)
{	
	//initial distance culling
	if (glm::distance(particleSystem->getOrigin(), windSource->getTranslation()) < MAXDISTANCE) {
		std::vector<Particle* > particles = particleSystem->getParticles();
		for (std::vector<Particle*>::iterator it = particles.begin(); it != particles.end(); ++it) {
			Particle * p = *it;
			//only react to wind if not colliding with another object
			if (!p->colliding) {
				reactToWindChannel(p, windSource);
			}
		}
	}
}

void CollisionDetector::checkParticleVsParticle(ParticleSystem * system1, ParticleSystem * system2)
{
	//distance culling
	if (glm::distance(system1->getOrigin(), system2->getOrigin()) < MAXDISTANCE * 2) {
		std::vector<Particle* > particles1 = system1->getParticles();
		std::vector<Particle* > particles2 = system2->getParticles();
		for (std::vector<Particle*>::iterator it = particles1.begin(); it != particles1.end(); ++it) {
			Particle * p1 = *it;
			for (std::vector<Particle*>::iterator it = particles2.begin(); it != particles2.end(); ++it) {
				Particle * p2 = *it;
				//simple distance check (bounding sphere unnecessary overhead as particles are meant to merge and overlap anyway)
				if (!p1->colliding && !p2->colliding &&  glm::distance(p1->getTranslation(), p2->getTranslation()) < p1->getDimensions() * p1->getScale().y + p2->getDimensions() * p2->getScale().y) {
					particleVsparticleReaction(p1, p2);
				}

			}
		}
	}
}



