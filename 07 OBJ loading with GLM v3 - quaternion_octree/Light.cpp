#include "Light.h"



Light::Light()
{
}

Light::Light(Shader * shader)
{
	this->shader = shader;
}


void Light::move(float position[3])
{
	for (int i = 0; i < 3; i++) {
		LightPos[1][i] += position[i];
	}
}

void Light::tint(float colour[3])
{
	for (int i = 0; i < 3; i++) {
		Light_Ambient[1][i] += colour[i];
	}
}

void Light::update() {
	//start using shader
	glUseProgram(shader->handle());

	//update lighting
	glUniform1i(glGetUniformLocation(shader->handle(), "maxLights"), maxLights);
	glUniform4fv(glGetUniformLocation(shader->handle(), "light_diffuse"), 1, Light_Diffuse);
	glUniform4fv(glGetUniformLocation(shader->handle(), "light_specular"), 1, Light_Specular);

	glUniform4fv(glGetUniformLocation(shader->handle(), ("light_ambient[0]")), 1, Light_Ambient[0]);
	glUniform4fv(glGetUniformLocation(shader->handle(), ("LightPos[0]")), 1, LightPos[0]);
	glUniform4fv(glGetUniformLocation(shader->handle(), ("light_ambient[1]")), 1, Light_Ambient[1]);
	glUniform4fv(glGetUniformLocation(shader->handle(), ("LightPos[1]")), 1, LightPos[1]);

	if (useAttenuation) {
		glUniform3fv(glGetUniformLocation(shader->handle(), "attenuation[0]"), 1, attenuation[0]);
		glUniform3fv(glGetUniformLocation(shader->handle(), "attenuation[1]"), 1, attenuation[1]);
	}
	else {
		float noAttenuation[3] = {1.0f,0.0f,0.0f};
		glUniform3fv(glGetUniformLocation(shader->handle(), "attenuation[0]"), 1, noAttenuation);
		glUniform3fv(glGetUniformLocation(shader->handle(), "attenuation[1]"), 1, noAttenuation);
	}

	//stop using shader
	glUseProgram(0);
}

Light::~Light()
{
}

