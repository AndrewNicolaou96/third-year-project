#include "Particle.h"

Particle::Particle()
{
}

Particle::Particle(glm::vec3 translation, glm::vec3 rotation, glm::vec3 scale, Shader * shader, Camera* camera) : GameObject(translation, rotation, scale, shader)
{
	this->camera = camera;
}

void Particle::createGeometry()
{
	glUseProgram(shader->handle());  //use the shader

	//declare arrays for vertex properties
	float verts[12]; //vertex co-ordinates
	float tex[8]; //texture co-ordinates
	unsigned int tris[6]; //triangle co-ordinates that make up the quad

	//set values of arrays
	verts[0] = -dimensions;   verts[1] = -dimensions;  verts[2] = -dimensions;
	verts[3] = -dimensions;   verts[4] = dimensions;  verts[5] = -dimensions;
	verts[6] = dimensions;   verts[7] = dimensions;  verts[8] = -dimensions;
	verts[9] = dimensions;   verts[10] = -dimensions;  verts[11] = -dimensions;

	tex[0] = 0.0f; tex[1] = 0.0f;
	tex[2] = 1.0f; tex[3] = 0.0f;
	tex[4] = 1.0f; tex[5] = 1.0f;
	tex[7] = 0.0f; tex[7] = 1.0f;

	//a quad is made up of 2 triangles
	tris[0] = 0; tris[1] = 1; tris[2] = 2;
	tris[3] = 0; tris[4] = 2; tris[5] = 3;

	// VAO allocation
	glGenVertexArrays(1, &vaoID);

	// First VAO setup
	glBindVertexArray(vaoID);

	//initialises data storage of vertex buffer object
	glGenBuffers(2, vboID);

	//bind verts
	glBindBuffer(GL_ARRAY_BUFFER, vboID[0]);
	glBufferData(GL_ARRAY_BUFFER, 4 * 3 * sizeof(GLfloat), verts, GL_STATIC_DRAW);

	//get the location of the attribute in the shader - we are interested in position
	GLint vertexLocation = glGetAttribLocation(shader->handle(), "in_Position");

	//set the vertex locations
	glVertexAttribPointer(vertexLocation, 3, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(vertexLocation);

	//bind tex
	glBindBuffer(GL_ARRAY_BUFFER, vboID[1]);
	glBufferData(GL_ARRAY_BUFFER, 8 * sizeof(GLfloat), tex, GL_STATIC_DRAW);

	//get the location of the attribute in the shader - we are interested in texture co-ordinates
	GLint textureLocation = glGetAttribLocation(shader->handle(), "in_TexCoord");

	//set the texture coordinates
	glVertexAttribPointer(textureLocation, 2, GL_FLOAT, GL_FALSE, 0, 0);
	glEnableVertexAttribArray(textureLocation);


	glBindBuffer(GL_ARRAY_BUFFER, 0);

	//bind triangles
	glGenBuffers(1, &ibo);
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);
	glBufferData(GL_ELEMENT_ARRAY_BUFFER, 2 * 3 * sizeof(unsigned int), tris, GL_STATIC_DRAW);

	//unbind
	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, 0);
	glEnableVertexAttribArray(0);
	glBindVertexArray(0);
}

void Particle::modelMatrixToInvertedViewMatrix(glm::mat4 viewMatrix)
{
	modelMatrix[0][0] = viewMatrix[0][0];
	modelMatrix[0][1] = viewMatrix[1][0];
	modelMatrix[0][2] = viewMatrix[2][0];
	modelMatrix[1][0] = viewMatrix[0][1];
	modelMatrix[1][1] = viewMatrix[1][1];
	modelMatrix[1][2] = viewMatrix[2][1];
	modelMatrix[2][0] = viewMatrix[0][2];
	modelMatrix[2][1] = viewMatrix[1][2];
	modelMatrix[2][2] = viewMatrix[2][2];
}

float Particle::getRemainingLifeTime()
{
	return remainingLifeTime;
}

float Particle::getCameraDistance()
{
	return cameraDistance;
}

float Particle::getDimensions()
{
	return dimensions;
}

glm::vec3 Particle::getOriginalVelocity()
{
	return originalVelocity;
}

void Particle::setSpin(float spin)
{
	this->spin = spin;
}

void Particle::setVelocity(glm::vec3 velocity)
{
	this->velocity = velocity;
}

void Particle::setOriginalVelocity(glm::vec3 velocity)
{
	this->originalVelocity = velocity;
	this->velocity = originalVelocity;
}

void Particle::setScaleFactor(glm::vec3 scaleFactor)
{
	this->scaleFactor = scaleFactor;
}

void Particle::setLifeTime(float lifeTime)
{
	this->lifeTime = lifeTime;
}

void Particle::setScale(glm::vec3 scale)
{
	this->scale = scale;
}

void Particle::setFramesBetweenAnimation(int frames)
{
	this->maxFramesBetweenAnimation = frames;
	this->framesBetweenAnimation = maxFramesBetweenAnimation;
}

void Particle::setAlphaCap(float alphaCap)
{
	this->alphaCap = alphaCap;
}

void Particle::setMaxScale(glm::vec3 maxScale)
{
	this->maxScale = maxScale;
}

float Particle::calculateTextureXOffset(int index)
{
	int column = index % numberOfTextureAtlasRows;
	return (float)column / (float)numberOfTextureAtlasRows;
}

float Particle::calculateTextureYOffset(int index)
{
	int row = index / numberOfTextureAtlasRows;
	return 1.0f - ((1.0f / numberOfTextureAtlasRows) + ((float)row / (float)numberOfTextureAtlasRows));
}


void Particle::resetLifeTime()
{
	remainingLifeTime = lifeTime;
}

void Particle::fadeOverLifeTime()
{
	//linear method
	Material_Alpha = abs(remainingLifeTime/ lifeTime) * alphaCap; //used to cap max alpha at a reasonable value

	float peakFrame = 100; //frame at which the peak alpha is displayed
	if (lifeTime - remainingLifeTime < peakFrame) {
		Material_Alpha = abs(((lifeTime - remainingLifeTime)/peakFrame) * alphaCap);
	}

}

void Particle::update()
{
	//exponential spin reduction
	float rotationIncrement = spin / (lifeTime + 1 - remainingLifeTime);

	//slow down animation over lifetime
	framesBetweenAnimation = maxFramesBetweenAnimation * (lifeTime / remainingLifeTime);

	glm::vec3 appliedVelocity;

	//velocity over lifetime
	appliedVelocity.y = velocity.y * (remainingLifeTime) /lifeTime; //slow down over lifetime
	appliedVelocity.x = velocity.x;
	appliedVelocity.z = velocity.z;


	//update transforms
    translation += appliedVelocity;
	rotation.z += rotationIncrement;
	scale += scaleFactor;
	if (scale.x > maxScale.x || scale.y > maxScale.y) {
		scale = maxScale;
	}

	//decrement lifetime by 1 frame
	remainingLifeTime--;

	//update distance from camera
	cameraDistance = glm::distance(camera->getTranslation(), translation);

	//fade paticle out over lifetime
	fadeOverLifeTime();

	//reset colliding to false for the next fram to re-check collision
	colliding = false;
}


void Particle::initBillboard(GLuint texName)
{
	this->texName = texName;
	createGeometry();
}

void Particle::draw(glm::mat4 viewMatrix)
{
	//increments texture animations frame
	if (frames > framesBetweenAnimation) {
		textureIndex++;
		if (textureIndex >= numberOfTextureAtlasColumns * numberOfTextureAtlasRows) {
			textureIndex = 0;
		}
		frames = 0;
	}
	//interpolate between frames
	float blendFactor = (float)frames / framesBetweenAnimation;
	frames++;
	//set matrix mode and use the shader
	glMatrixMode(GL_MODELVIEW);
	glDepthMask(false);
	glUseProgram(shader->handle());

	//pass material properties to shader
	glUniform4fv(glGetUniformLocation(shader->handle(), "material_ambient"), 1, Material_Ambient);
	glUniform4fv(glGetUniformLocation(shader->handle(), "material_diffuse"), 1, Material_Diffuse);
	glUniform4fv(glGetUniformLocation(shader->handle(), "material_specular"), 1, Material_Specular);
	glUniform1f(glGetUniformLocation(shader->handle(), "material_shininess"), Material_Shininess);
	glUniform1f(glGetUniformLocation(shader->handle(), "material_alpha"), Material_Alpha);
	glUniform1f(glGetUniformLocation(shader->handle(), "blend"), blendFactor);
	glUniform1f(glGetUniformLocation(shader->handle(), "numberOfRows"), numberOfTextureAtlasRows);
	glUniform2f(glGetUniformLocation(shader->handle(), "texOffset1"), calculateTextureXOffset(textureIndex), calculateTextureYOffset(textureIndex));
	//reset texture index count once all frames have been rendered
	if (textureIndex < numberOfTextureAtlasColumns * numberOfTextureAtlasRows -1) {
		glUniform2f(glGetUniformLocation(shader->handle(), "texOffset2"), calculateTextureXOffset(textureIndex + 1), calculateTextureYOffset(textureIndex + 1));
	}
	else {
		glUniform2f(glGetUniformLocation(shader->handle(), "texOffset2"), calculateTextureXOffset(textureIndex), calculateTextureYOffset(textureIndex));
	}


	//reset model matrix
	modelMatrix = glm::mat4(1.0f);

	//perform translations
	modelMatrix = glm::translate(modelMatrix, translation);

	//make billboard effect
	modelMatrixToInvertedViewMatrix(viewMatrix);

	//perform rotations
	modelMatrix = glm::rotate(modelMatrix, rotation.x, glm::vec3(1, 0, 0));
	modelMatrix = glm::rotate(modelMatrix, rotation.y, glm::vec3(0, 1, 0));
	modelMatrix = glm::rotate(modelMatrix, rotation.z, glm::vec3(0, 0, 1));

	//perfrom scales
	modelMatrix = glm::scale(modelMatrix, scale);

	//combine model matrix with view natrix to be passed to shader
	glm::mat4 modelViewMatrix = viewMatrix * modelMatrix;

	//pass model matrix to shader
	glUniformMatrix4fv(glGetUniformLocation(shader->handle(), "ModelMatrix"), 1, GL_FALSE, &modelMatrix[0][0]);

	//calculate normal matrix and pass it to shader
	glm::mat3 normalMatrix = glm::inverseTranspose(glm::mat3(modelViewMatrix));
	glUniformMatrix3fv(glGetUniformLocation(shader->handle(), "NormalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);

	//draw objects
	glBindVertexArray(vaoID);		// select VAO

	glBindBuffer(GL_ELEMENT_ARRAY_BUFFER, ibo);

	glDrawElements(GL_TRIANGLES, 2 * 3, GL_UNSIGNED_INT, 0);

	// Done
	glBindBuffer(GL_ARRAY_BUFFER, 0);  //unbind the buffer

	glBindVertexArray(0); //unbind the vertex array object

	//stop using shader
	glUseProgram(0);
	glDepthMask(true);
}


Particle::~Particle()
{
}
