#pragma once
#include "GameObject.h"
/*Object representing a wind channel - contains an OBJ file such as a fan to represent source of wind*/
class Wind :
	public GameObject
{
private:
	float height;	//the distance between the wind origin and the top or bottom of the channel
	glm::vec3 maxChannelVelocity; //maximum valocity of wind channel as distance tends to 0
	float range;	// how far the effect reaches
	float spin = 0; // rotation of the fan blades
	float rotationMultiplier = 20; //increase or decrease fan rotation without increasing physical reaction

public:
	Wind();
	Wind(glm::vec3 translation, glm::vec3 rotation, glm::vec3 scale, Shader* shader, float height, glm::vec3 maxChannelVelocity, float range);
	
	//ACCESSORS/MUTATORS
	float getHeight();
	glm::vec3 getMaxChannelVelocity();
	float getRange();
	float getSpin();

	void setSpin(float spin);

	//draw the wind source model
	void draw(glm::mat4 viewMatrix);

	~Wind();
};

