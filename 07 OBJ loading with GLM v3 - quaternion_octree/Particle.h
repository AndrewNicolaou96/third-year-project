#pragma once
#include "GameObject.h"
#include "Camera.h"
/*The object which represents an individual partitcle entity*/
class Particle
	: public GameObject
{
private:
	/**********************************FIELDS***************************************/

	//OpenGL fields
	unsigned int vaoID; //vertex array object
	unsigned int vboID[2]; //vertex buffer object
	GLuint ibo; //identifier for the triangle indices
	GLuint texName = 0; //where the texture is located on GPU
	float dimensions = 0.5f; //dimensions of square

	//texture animation
	int textureIndex = 0;
	int numberOfTextureAtlasRows = 8;
	int numberOfTextureAtlasColumns = 8;
	int maxFramesBetweenAnimation = 5;
	int framesBetweenAnimation = 5; //number of real frames to count until next animation frame
	int frames = 0;	//current frame in the count

	//particle properties
	float alphaCap = 0.3f;
	float lifeTime;	//the total lifetime of a particle - measured in frames
	float remainingLifeTime; //the liftime left unitl death
	glm::vec3 originalVelocity;//the velocity that the particle starts at

	float spin; //amount to increment zRotation by
	glm::vec3 scaleFactor; //proportion the particles grows or shrinks by e.g 0.1 is 10% growth and -0.1 is 10% shrinkage
	Camera* camera; 
	float cameraDistance; //used to sort particles for alpha blending
	glm::vec3 maxScale = glm::vec3(8.0f, 8.0f, 8.0f); //max size that particle can become
	/**********************************METHODS***************************************/
	//creates a textured quad
	void createGeometry();
	//used to create billboard effect by making the modelmatrix rotation
	//be the inverse of the viewmatrix
	void modelMatrixToInvertedViewMatrix(glm::mat4 viewMatrix);
public:
	/*********************************FIELDS*******************************************/
	bool emit = false;
	bool colliding = false;
	glm::vec3 velocity; //vector to translate the particle by per frame
	
	/******************************METHODS*********************************************/
    //constructors
	Particle();
	Particle(glm::vec3 translation, glm::vec3 rotation, glm::vec3 scale, Shader* shader, Camera* camera);

	//ACCESORS/MUTATORS
	float getRemainingLifeTime();
	float getCameraDistance();
	float getDimensions();
	glm::vec3 getOriginalVelocity();

	void setSpin(float spin);
	void setVelocity(glm::vec3 velocity);
	void setOriginalVelocity(glm::vec3 velocity);
	void setScaleFactor(glm::vec3 scaleFactor);
	void setLifeTime(float lifeTime);
	void setScale(glm::vec3 scale);
	void setFramesBetweenAnimation(int frames);
	void setAlphaCap(float alphaCap);
	void setMaxScale(glm::vec3 maxScale);

	//used to calculate the offset of the current desired texture in the texture atlas
	float calculateTextureXOffset(int index);
	float calculateTextureYOffset(int index);

	//lifetime functions
	void resetLifeTime();
	void fadeOverLifeTime(); //reduces alpha value over lifetime

	void update(); //update the particles position and attributes
	//initialise a billboard
	void initBillboard(GLuint texName);
	//overrides base draw
	void draw(glm::mat4 viewMatrix);

	//destructor
	~Particle();
};

