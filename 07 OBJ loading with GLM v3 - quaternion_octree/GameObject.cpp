#include "GameObject.h"

GameObject::GameObject()
{
}

GameObject::GameObject(glm::vec3 translation, glm::vec3 rotation, glm::vec3 scale, Shader* shader)
{
	this->translation = translation;
	this->rotation = rotation;
	this->scale = scale;
	this->shader = shader;
}

glm::vec3 GameObject::getTranslation()
{
	return translation;
}

glm::vec3 GameObject::getRotation()
{
	return rotation;
}

glm::vec3 GameObject::getScale()
{
	return scale;
}

float GameObject::getBoundingSphere()
{
	return boundingSphereRadius;
}

ThreeDModel GameObject::getModel()
{
	return model;
}

void GameObject::setTranslation(glm::vec3 translation)
{
	this->translation = translation;
}

void GameObject::setBoundingSphere(float radius)
{
	this->boundingSphereRadius = radius;
}

void GameObject::setAlpha(float alpha)
{
	this->Material_Alpha = alpha;
}

void GameObject::transform(glm::vec3 translation, glm::vec3 rotation, glm::vec3 scale)
{
	//perform all transformations
	this->translation = this->translation + translation;
	this->rotation = this->rotation + rotation;
	this->scale = scale;
	boundingSphereRadius = boundingSphereRadius * (scale.x + scale.y + scale.z) / 3;
}

void GameObject::calculateBoundingSphere()
{
	//calculates radius of sphere by equaling it to half the diameter of the bounding box object
	boundingSphereRadius = model.theBBox.boxWidthX / 2;
}

void GameObject::initOBJ(OBJLoader objLoader, char* modelName)
{
	glUseProgram(shader->handle());  // use the shader

	glEnable(GL_TEXTURE_2D);

	cout << " loading model " << endl;
	if (objLoader.loadModel(modelName, model))//returns true if the model is loaded, puts the model in the model parameter
	{
		cout << " model loaded " << endl;
		model.calcVertNormalsUsingOctree();  //the method will construct the octree if it hasn't already been created.


		//turn on VBO by setting useVBO to true in threeDmodel.cpp default constructor - only permitted on 8 series cards and higher
		model.initDrawElements();
		model.initVBO(shader);
		model.deleteVertexFaceData();

	}
	else
	{
		cout << " model failed to load " << endl;
	}
	//Constructs the AABB
	double minX, minY, minZ, maxX, maxY, maxZ;
	model.calcCentrePoint();
	model.calcBoundingBox(minX, minY, minZ, maxX, maxY, maxZ);
	calculateBoundingSphere();
}

void GameObject::draw(glm::mat4 viewMatrix)
{
	//set matrix mode and use the shader
	glMatrixMode(GL_MODELVIEW);
	glUseProgram(shader->handle());

	//pass material properties to shader
	glUniform4fv(glGetUniformLocation(shader->handle(), "material_ambient"), 1, Material_Ambient);
	glUniform4fv(glGetUniformLocation(shader->handle(), "material_diffuse"), 1, Material_Diffuse);
	glUniform4fv(glGetUniformLocation(shader->handle(), "material_specular"), 1, Material_Specular);
	glUniform1f(glGetUniformLocation(shader->handle(), "material_shininess"), Material_Shininess);
	
	/*Use this to allow transparent objects*/
	//glUniform1f(glGetUniformLocation(shader->handle(), "material_alpha"), Material_Alpha);


	//reset model matrix
	modelMatrix = glm::mat4(1.0f);

	//perform translations
	modelMatrix = glm::translate(modelMatrix, translation);

	//perform rotations
	modelMatrix = glm::rotate(modelMatrix, rotation.x, glm::vec3(1, 0, 0));
	modelMatrix = glm::rotate(modelMatrix, rotation.y, glm::vec3(0, 1, 0));
	modelMatrix = glm::rotate(modelMatrix, rotation.z, glm::vec3(0, 0, 1));

	//perfrom scales
	modelMatrix = glm::scale(modelMatrix, scale);

	//combine model matrix with view natrix to be passed to shader
	glm::mat4 modelViewMatrix = viewMatrix * modelMatrix;

	//pass modelview matrix to shader
	glUniformMatrix4fv(glGetUniformLocation(shader->handle(), "ModelMatrix"), 1, GL_FALSE, &modelMatrix[0][0]);

	//calculate normal matrix and pass it to shader
	glm::mat3 normalMatrix = glm::inverseTranspose(glm::mat3(modelViewMatrix));
	glUniformMatrix3fv(glGetUniformLocation(shader->handle(), "NormalMatrix"), 1, GL_FALSE, &normalMatrix[0][0]);

	//draw model
	model.drawElementsUsingVBO(shader);

	//stop using shader
	glUseProgram(0);
}

GameObject::~GameObject()
{
}
