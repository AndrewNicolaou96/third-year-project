#include "Camera.h"



Camera::Camera()
{
}

Camera::Camera(glm::vec3 translation, glm::vec3 rotation, glm::vec3 scale, Shader * shader): GameObject(translation, rotation, scale, shader)
{
}

void Camera::setFocus(GameObject* focus)
{
	this->focus = focus;
}

void Camera::update()
{
	//rotate camera while still viewing focus
	float yRotationInRadians = focus->getRotation().y * valueOFPi / 180;

	//rotate camera around focus axis
	glm::vec3 cameraOffset = glm::vec3(3 * sin(yRotationInRadians), 1, 3 * cos(yRotationInRadians));
	translation = focus->getTranslation() + cameraOffset;
	viewMatrix = glm::lookAt(translation, focus->getTranslation(), glm::vec3(0, 1, 0));
}


Camera::~Camera()
{
}
